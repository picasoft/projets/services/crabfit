#! /usr/bin/env bash

export DATABASE_URL=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@crabfit-db/$POSTGRES_DB
/api
