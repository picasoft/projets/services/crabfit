# Crab.fit

Application pour sonder des dates comme framadate mais plus adapté à avoir beaucoup de crénaux.

## Installation

Le service est décomposé en un frontend, un backend et une base de données.
Les technos du front et back étant très différentes, les deux ont une image docker dédiée.

Il suffit néanmoins de lancer le service avec un 

```bash
docker compose up
```

Les identifiants pour la base de donnée seront chargés depuis `secrets/crabfit-db.secrets` qui est à adapter depuis `secrets/crabfit-db.secrets.example`.
